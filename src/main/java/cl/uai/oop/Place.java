/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import java.util.Date;

/**
 *
 * @author Sebastian
 */
// no puede extenderse el Comparator, solo implementarse.
public class Place {

    public String nombre;
    public String descripcion;
    public TypePlace tplace;
    // Habia que crear clase enum
    private PlaceLocation ubicacion;
    public Date horarioAper;
    public Date horarioTerm;

    public Place(PlaceLocation ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Place(String nombre, String descripcion, TypePlace tlugar, PlaceLocation ubicacion, Date horarioAper, Date horarioTerm) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tplace = tlugar;
        this.ubicacion = ubicacion;
        this.horarioAper = horarioAper;
        this.horarioTerm = horarioTerm;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Enum getTypePlace() {
        return tplace;
    }

    public void setTypePlace(TypePlace tplace) {
        this.tplace = tplace;
    }

    public PlaceLocation getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(PlaceLocation ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Date getHorarioAper() {
        return horarioAper;
    }

    public void setHorarioAper(Date horarioAper) {
        this.horarioAper = horarioAper;
    }

    public Date getHorarioTerm() {
        return horarioTerm;
    }

    public void setHorarioTerm(Date horarioTerm) {
        this.horarioTerm = horarioTerm;
    }

    @Override
    // este equals compara objeto, habria que poner obj intanceof String
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            String lugar = (String) obj;
            return this.getNombre().equals(lugar);
        }
        if (obj instanceof Place) {
            Place lugar = (Place) obj;
            return this.getNombre().equals(lugar.getNombre());
        }
        return false;
    }
}
