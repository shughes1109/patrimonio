/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.serverMod;

import cl.uai.oop.ListaLugares;
import cl.uai.oop.Place;
import cl.uai.oop.PlaceLocation;
import cl.uai.oop.Utils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Sebastian
 */
public class ThreadServerCliente extends Thread {

    Socket clientSocket;

    public ThreadServerCliente(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {
            InputStream is = clientSocket.getInputStream();
            OutputStream os = clientSocket.getOutputStream();
            DataInputStream in = new DataInputStream(is);
            DataOutputStream out = new DataOutputStream(os);
            String command = in.readUTF();
            String[] parameters = command.split(";");
            ListaLugares lista = new ListaLugares();
            int option = Integer.parseInt(parameters[0]);
            if (option == 1) {
                PlaceLocation pl = new PlaceLocation(Double.parseDouble(parameters[1]), Double.parseDouble(parameters[2]));
                // esta lista se supone que esta creada previamente, asi que no es null
                //Place pactual=new Place(pl);
                // envio de datos con writeUTF.
                //TODO: get the nearest places sorted by distance

                ArrayList<Place> listaord = lista.getSortedPlaceByDistance(pl, lista);
                out.writeUTF(String.valueOf(listaord.size()));
                for (Place lug : listaord) {
                    double dista = Utils.distance(lug.getUbicacion(), pl);
                    out.writeUTF(String.format(lug.getNombre(), ";", dista));
                }
                out.flush();

                //TODO: return the list of place to client socket
            } else if (option == 2) {
                String placeName = parameters[1];
                String descripcionLugar = lista.busquedaLineal(placeName);

                if (descripcionLugar.equals("")) {
                    out.writeUTF("lugar no encontrado. intentelo denuevo");
                } else {
                    out.writeUTF(descripcionLugar);
                }

                out.flush();
                //TODO: return the place to client socket
            }
        } catch (IOException e) {
        }
    }
}
