/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import java.util.Comparator;

/**
 *
 * @author Sebastian
 */
public class PlaceSortByDistance implements Comparator<Place> {

    PlaceLocation pla;

    public PlaceSortByDistance(PlaceLocation pla) {
        this.pla = pla;

    }

    @Override
    public int compare(Place p1, Place p2) {
        Double dis1 = Utils.distance(pla, p1.getUbicacion());
        Double dis2 = Utils.distance(pla, p2.getUbicacion());
        if (dis1 < dis2) {
            return -1;
        } else if (dis1 > dis2) {
            return 1;
        } else {
            return 0;
        }
    }

}
