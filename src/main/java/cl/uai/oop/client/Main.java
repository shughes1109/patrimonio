/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.client;

import cl.uai.oop.PlaceLocation;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main {

    public static void main(String[] args) {
        try {
            String hostName = args[0];
            int portNumber = Integer.parseInt(args[1]);
            Socket mySocket = new Socket(hostName, portNumber);
            InputStream is = mySocket.getInputStream();
            OutputStream os = mySocket.getOutputStream();
            Scanner myScanner = new Scanner(System.in);
            int option = -1;
            while (true) {
                System.out.println("Bienvenido. Ingrese la opción deseada.");
                System.out.println("1 para listar los lugares cercanos");
                System.out.println("2 para encontrar información de un lugar");
                System.out.println("0 para salir");
                option = myScanner.nextInt();
                if (option == 0) {
                    break;
                } else if (option == 1) {
                    PlaceLocation pl = PlaceLocation.currentLocation();
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(String.format("1;%s", pl.toString()));
                    dos.flush();
                    // listado de datos, captura.
                    DataInputStream dis = new DataInputStream(is);
                    // esta no es la forma mas economica probablemente, pero mandando el primer dato que es el tamaño del listado, que solo conoce el servidor, la recepcion
                    // la hago en base a ese numero, permitiendo un numero de capturas de igual de strings, que envian el nombre dle lugar y su distancia a la ubicacion actual, ya ordenado
                    int numerolist = Integer.parseInt(dis.readUTF());
                    String command = "";
                    for (int i = 1; i < numerolist; i++) {
                        command = dis.readUTF();
                    }
                    String[] parameters = command.split(";");
                    for (String par : parameters) {
                        System.out.print(par);
                        System.out.print("------");
                    }
                    System.out.println();

                    //devolver el nombre del lugar y la distancia.
                } else if (option == 2) {
                    String nombre = "nombre";
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(String.format("2;%s", nombre));
                    dos.flush();

                    DataInputStream dis = new DataInputStream(is);
                    String command1 = dis.readUTF();
                    System.out.println(nombre + "_" + command1);

                } else {
                    System.out.println("Opción inválida");
                }
            }
        } catch (IOException | GeoIp2Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
