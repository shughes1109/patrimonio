/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

/**
 *
 * @author Sebastian
 */
import java.util.ArrayList;
import java.util.Collections;

public class ListaLugares {

    ArrayList<Place> pllist = new ArrayList<>();

    public ListaLugares() {
    }

    public ListaLugares(ArrayList<Place> pllist) {
        this.pllist = pllist;
    }

    public ArrayList<Place> getPlList() {
        return this.pllist;
    }

    public void nuevoLugar(ArrayList<Place> pllist, Place nlug) {

        pllist.add(nlug);
    }

    //Tengo duda aca... el dato que el cliente envia es la ubicacion, lo que estoy incluyendo como argumento 
    // sin embargo, 
    public ArrayList<Place> getSortedPlaceByDistance(PlaceLocation pla, ListaLugares plist) {
        ArrayList<Place> plord;

        Collections.sort(plist.pllist, new PlaceSortByDistance(pla));
        plord = plist.pllist;
        return plord;
    }

    public String busquedaLineal(String lugar) {
        int pos = this.pllist.indexOf(lugar);
        Place pl;
        String resp;
        if (pos != -1) {
            pl = pllist.get(pos);
            resp = pl.descripcion;
        } else {
            resp = "";
        }
        return resp;
    }

}
