/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gohucan
 */
public class UtilsTest {

    private final Faker faker = new Faker();
    private final List<PlaceLocation> places = new ArrayList<>(100);

    public UtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        for (int i = 0; i < 100; i++) {
            double latitude = Double.parseDouble(faker.address().latitude().replace(',', '.'));
            double longitude = Double.parseDouble(faker.address().longitude().replace(',', '.'));
            PlaceLocation pl = new PlaceLocation(latitude, longitude);
            places.add(pl);

        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of distance method, of class Utils.
     *
     * @param places
     */
    @Test
    public void testDistance(ArrayList<PlaceLocation> places) {
        double latitude = Double.parseDouble(faker.address().latitude().replace(',', '.'));
        double longitude = Double.parseDouble(faker.address().longitude().replace(',', '.'));
        PlaceLocation pla = new PlaceLocation(latitude, longitude);
        ArrayList<Double> distancias = new ArrayList<>(100);
        for (PlaceLocation pl : places) {
            assert (Utils.distance(pla, pl) > 0);
            distancias.add(Utils.distance(pla, pl));

        }

    }

}
